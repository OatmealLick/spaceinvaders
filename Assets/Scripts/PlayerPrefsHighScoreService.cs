using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace SpaceInvaders
{

public class PlayerPrefsHighScoreService : IHighScoreService
{
    private const string CurrentScoreIdKey = "current_score_id";
    private const string ScoreKey = "score";
    int _highScoreId;

    public PlayerPrefsHighScoreService()
    {
        _highScoreId = PlayerPrefs.GetInt(CurrentScoreIdKey, 0);
    }
    
    public List<HighScore> GetHighScores()
    {
        return Enumerable.Range(0, _highScoreId)
            .Select(id => BuildScoreKey(id))
            .Select(key => PlayerPrefs.GetString(key, ""))
            .Where(highScore => highScore != "")
            .Select(highScoreString => {
                var highScoreParts = highScoreString.Split(',');
                return new HighScore(Int32.Parse(highScoreParts[0]), highScoreParts[1]);
            })
            .ToList();
    }

    public void SaveHighScore(int score)
    {
        var date = DateTime.Now.ToString();
        var highScoreString = $"{score},{date}";
        PlayerPrefs.SetString(BuildScoreKey(_highScoreId), highScoreString);
        ++_highScoreId;
        PlayerPrefs.SetInt(CurrentScoreIdKey, _highScoreId);
    }

    public void DeleteAllHighScores()
    {
        Enumerable.Range(0, _highScoreId)
            .Select(id => BuildScoreKey(id))
            .ToList()
            .ForEach(key => PlayerPrefs.DeleteKey(key));
    }

    string BuildScoreKey(int score)
    {
        return $"{ScoreKey}{score}";
    }

}}