﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace SpaceInvaders
{
    public class HighScoreComponent : MonoBehaviour
    {
        HighScoreEntity _entity;

        [Inject]
        public void Construct(HighScoreEntity entity)
        {
            _entity = entity;
        }

        public void ResetScores()
        {
            _entity.ResetScores();
        }
    }
}