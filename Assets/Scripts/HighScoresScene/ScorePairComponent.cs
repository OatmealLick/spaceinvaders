﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
namespace SpaceInvaders
{

    public class ScorePairComponent : MonoBehaviour
    {
        [SerializeField]
        TextMeshProUGUI titleText;
        [SerializeField]
        TextMeshProUGUI scoreValueText;
        [SerializeField]
        TextMeshProUGUI scoreDateText;

        public void Setup(string title, HighScore highScore)
        {
            titleText.text = $"{title}.";
            scoreValueText.text = highScore.Score.ToString();
            scoreDateText.text = highScore.Date;
        }
    }
}