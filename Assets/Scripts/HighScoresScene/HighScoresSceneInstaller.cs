using UnityEngine;
using Zenject;

namespace SpaceInvaders
{
    public class HighScoresSceneInstaller : MonoInstaller
    {
        [SerializeField]
        Transform scoresParent;
        [SerializeField]
        ScorePairComponent scorePairPrefab;

        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<HighScoreEntity>().AsSingle().WithArguments(scoresParent, scorePairPrefab);
        }
    }
}