﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using TMPro;
using System;
using System.Linq;

namespace SpaceInvaders
{
    public class HighScoreEntity : IInitializable, IComparer<HighScore>
    {
        IHighScoreService _highScoreService;
        ScorePairComponent _scorePairPrefab;
        Transform _scoresParent;
        const float height = 0.7f;
        const int highScoresNumber = 10;

        [Inject]
        public void Construct(IHighScoreService highScoreService, Transform scoresParent, ScorePairComponent scorePairPrefab)
        {
            _highScoreService = highScoreService;
            _scoresParent = scoresParent;
            _scorePairPrefab = scorePairPrefab;
        }

        public void Initialize()
        {
            CreateScores();
        }

        void CreateScores()
        {
            var highScores = _highScoreService.GetHighScores();
            highScores.Sort(this);
            highScores.Take(highScoresNumber)
                .Select((v, i) => new { index = i, value = v })
                .ToList()
                .ForEach(pairStruct =>
                {
                    var position = _scoresParent.position + new Vector3(0f, -pairStruct.index * height, 0f);
                    var pair = UnityEngine.Object.Instantiate(_scorePairPrefab, position, Quaternion.identity, _scoresParent);
                    pair.Setup((pairStruct.index + 1).ToString(), pairStruct.value);
                });
        }

        public void ResetScores()
        {
            foreach (Transform child in _scoresParent)
            {
                UnityEngine.Object.Destroy(child.gameObject);
            }
            _highScoreService.DeleteAllHighScores();
        }

        public int Compare(HighScore x, HighScore y)
        {
            return y.Score.CompareTo(x.Score);
        }
    }
}