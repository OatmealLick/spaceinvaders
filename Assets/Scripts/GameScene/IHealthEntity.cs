using System;

namespace SpaceInvaders
{
    public interface IHealthEntity
    {
        event Action Died;
        event Action Damaged;
        TakingDamageResult TakeDamage();
        void StartTakingDamage();
        void StopTakingDamage();
        int GetHealth();
        void Die();
    }

    public enum TakingDamageResult
    {
        Alive, Dead, Invincible
    }
}