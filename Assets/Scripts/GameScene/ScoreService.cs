using System;
using Zenject;

namespace SpaceInvaders
{
    public class ScoreService : IScoreService
    {
        int score;

        public event Action<int> Updated;

        public ScoreService()
        {
            score = 0;
        }

        public void AddPoint()
        {
            ++score;
            Updated?.Invoke(score);
        }

        public int GetScore()
        {
            return score;
        }

        public void ResetScore()
        {
            score = 0;
            Updated?.Invoke(score);
        }
    }
}