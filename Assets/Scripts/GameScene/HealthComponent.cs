﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Zenject;
namespace SpaceInvaders
{

    public class HealthComponent : MonoBehaviour
    {
        [SerializeField]
        TextMeshProUGUI _healthText;

        IHealthEntity _healthEntity;

        [Inject]
        public void Construct(IHealthEntity healthEntity)
        {
            _healthEntity = healthEntity;
            _healthEntity.Damaged += UpdateHealthText;
            UpdateHealthText();
        }

        void OnDestroy()
        {
            _healthEntity.Damaged -= UpdateHealthText;
        }

        void UpdateHealthText()
        {
            _healthText.text = _healthEntity.GetHealth().ToString();
        }
    }
}