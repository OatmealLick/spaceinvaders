﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceInvaders
{
    public interface IPlayerMovementEntity
    {
        event Action<Quaternion> Rotate;
        Vector3 Move(Vector3 position);
        void DisableMovement();
        void EnableMovement();
    }
}