﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace SpaceInvaders
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField]
        GameObject projectilePrefab;
        [SerializeField]
        GameObject shield;
        [SerializeField]
        Collider playerCollider;

        PlayerEntity _entity;

        [Inject]
        void Construct(PlayerEntity entity)
        {
            _entity = entity;
            _entity.PositionChanged += Move;
            _entity.RotationChanged += Rotate;
            _entity.StartBeingInvincible += StartBeingInvincible;
            _entity.StopBeingInvincible += StopBeingInvincible;
        }

        void OnDestroy()
        {
            _entity.PositionChanged -= Move;
            _entity.RotationChanged -= Rotate;
            _entity.StartBeingInvincible -= StartBeingInvincible;
            _entity.StopBeingInvincible -= StopBeingInvincible;
        }

        void Move(Vector3 position)
        {
            transform.localPosition = position;
        }

        void Rotate(Quaternion rotation)
        {
            transform.localRotation = rotation;
        }

        void StartBeingInvincible()
        {
            playerCollider.enabled = false;
            shield.SetActive(true);
        }

        void StopBeingInvincible()
        {
            playerCollider.enabled = true;
            shield.SetActive(false);
        }

        void OnTriggerEnter(Collider other)
        {
            _entity.HandleCollision(other.gameObject);
        }
    }
}