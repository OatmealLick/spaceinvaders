using System.Collections.Generic;
using System.Linq;

namespace SpaceInvaders
{
    public class LowestPositionsSelector : ILowestPositionsSelector
    {
        public List<Wave.Position> Select(List<Wave.Position> positions)
        {
            var readyToFire = new Dictionary<int, Wave.Position>();
            foreach (var id in positions)
            {
                if (!readyToFire.ContainsKey(id.Y) || readyToFire[id.Y].X > id.X)
                {
                    readyToFire.Add(id.Y, id);
                }
            }
            return readyToFire.Values.ToList();
        }
    }
}