﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace SpaceInvaders
{
    public class ProjectileController : MonoBehaviour
    {
        float _movementSpeed;

        [Inject]
        public void Construct(float movementSpeed)
        {
            _movementSpeed = movementSpeed;
        }

        void Update()
        {
            transform.localPosition += new Vector3(0f, 0f, _movementSpeed * Time.deltaTime);

            if (Vector3.Distance(transform.localPosition, Vector3.zero) >= 30f)
            {
                Destroy(gameObject);
            }
        }

        public class Factory : PlaceholderFactory<float, ProjectileController>
        {
        }
    }
}