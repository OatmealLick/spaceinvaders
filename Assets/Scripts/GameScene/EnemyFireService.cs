using UnityEngine;

namespace SpaceInvaders
{
    public class EnemyFireService : IFireService
    {
        readonly ProjectileController.Factory _projectileFactory;
        readonly Enemy.Settings _settings;

        public EnemyFireService(ProjectileController.Factory projectileFactory, Enemy.Settings settings)
        {
            _projectileFactory = projectileFactory;
            _settings = settings;
        }

        public void Fire(Vector3 startingPosition)
        {
            var negatedSpeed = -_settings.projectileSpeed;
            var projectile = _projectileFactory.Create(negatedSpeed);
            projectile.transform.localPosition = startingPosition;
            projectile.gameObject.tag = Tags.EnemyProjectile;
        }
    }
}