using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Zenject;

namespace SpaceInvaders
{
    public class Wave : IInitializable, ITickable
    {
        public event Action WaveDestroyed;
        public Dictionary<Position, Enemy> enemies;
        Tween firingTween;
        Tween movingDownTween;
        readonly Settings _settings;
        readonly Enemy.Factory _enemyFactory;
        readonly IScoreService _scoreService;
        readonly ILowestPositionsSelector _lowestPositionsSelector;
        readonly IHealthEntity _healthEntity;
        readonly IAssetManager _assetManager;
        readonly IEnemySelector _enemySelector;
        const float EnemyHorizontalOffset = 2.5f;
        const float EnemyVerticalOffset = 1.7f;

        public Wave(Settings settings,
                Enemy.Factory enemyFactory,
                IScoreService scoreService,
                ILowestPositionsSelector lowestPositionsSelector,
                IHealthEntity healthEntity,
                IAssetManager assetManager,
                IEnemySelector enemySelector)
        {
            _settings = settings;
            _enemyFactory = enemyFactory;
            _scoreService = scoreService;
            _lowestPositionsSelector = lowestPositionsSelector;
            _healthEntity = healthEntity;
            _assetManager = assetManager;
            _enemySelector = enemySelector;
        }

        public void Initialize()
        {
            CreateEnemies();
            ScheduleFiring();
            ScheduleMovingDown();
            _healthEntity.Died += DisableSystems;
        }

        public void DisableSystems()
        {
            firingTween.Kill();
            movingDownTween.Kill();
        }

        void CreateEnemies()
        {
            enemies = new Dictionary<Position, Enemy>();
            for (int i = 0; i < _settings.columns; ++i)
            {
                for (int j = 0; j < _settings.rows; ++j)
                {
                    var id = new Position(j, i);
                    var position = BuildPosition(i, j);
                    var enemy = _enemyFactory.Create(position, id);
                    var type = _enemySelector.Select(j, i);
                    InstantiateEnemyComponent(type, position, enemy);
                    enemy.Damaged += EnemyDestroyed;
                    enemy.EndReached += EndGame;
                    enemies[id] = enemy;
                }
            }
        }

        async void InstantiateEnemyComponent(Enemy.Type type, Vector3 position, Enemy enemy)
        {
            var enemyGameObject = await _assetManager.InstantiateEnemy(type, position, Quaternion.identity, _enemyFactory._enemyParentTransform);
            enemyGameObject.GetComponent<EnemyController>().SetupEntity(enemy);
        }

        Vector3 BuildPosition(int i, int j)
        {
            var positionX = EnemyHorizontalOffset * (i - ((float)_settings.columns) / 2);
            var positionZ = EnemyVerticalOffset * j;
            var position = new Vector3(positionX, 0f, positionZ) + _enemyFactory._enemyParentTransform.position;
            return position;
        }

        void EndGame()
        {
            _healthEntity.Die();
        }

        void ScheduleFiring()
        {
            firingTween = Repeat(Fire, _settings.firingIntervalInSeconds);
        }

        void ScheduleMovingDown()
        {
            movingDownTween = Repeat(MoveDown, _settings.movementSegmentInSeconds);
        }

        public void Tick()
        {
            foreach (var enemy in enemies.Values)
            {
                enemy.Tick();
            }
        }

        public void MoveDown()
        {
            foreach (var enemy in enemies.Values)
            {
                enemy.MoveDown(EnemyVerticalOffset);
            }
        }

        void Fire()
        {
            var lowest = _lowestPositionsSelector.Select(enemies.Keys.ToList());
            var enemyNumber = lowest.ElementAt(UnityEngine.Random.Range(0, lowest.Count));
            enemies[enemyNumber].Fire();
        }

        public void EnemyDestroyed(Position id, GameObject collided)
        {
            _scoreService.AddPoint();
            var enemy = enemies[id];
            enemy.Damaged -= EnemyDestroyed;
            enemy.EndReached -= EndGame;
            enemies.Remove(id);
            if (enemies.Count == 0)
            {
                Destroy();
            }
        }

        void Destroy()
        {
            DisableSystems();
            WaveDestroyed?.Invoke();
            _healthEntity.Died -= DisableSystems;
        }

        Tween Repeat(Action callback, float interval)
        {
            return DOTween.Sequence().InsertCallback(interval, () => callback()).SetLoops(-1);
        }

        [Serializable]
        public class Settings
        {
            public List<AssetReference> enemyPrefabs;
            [Range(3, 15)]
            public int columns = 9;
            [Range(1, 5)]
            public int rows = 3;
            [Range(2f, 30f)]
            public float movementSegmentInSeconds = 5f;
            [Range(0.5f, 7f)]
            public float firingIntervalInSeconds = 3f;

        }

        public class Factory : PlaceholderFactory<Wave> { }

        public class Position
        {
            public int X { get; private set; }
            public int Y { get; private set; }

            public Position(int x, int y)
            {
                X = x;
                Y = y;
            }

            public override string ToString()
            {
                return $"Position: x: {X}, y: {Y}";
            }

            public override bool Equals(object obj)
            {
                return obj is Position position &&
                       X == position.X &&
                       Y == position.Y;
            }

            public override int GetHashCode()
            {
                int hashCode = 1861411795;
                hashCode = hashCode * -1521134295 + X.GetHashCode();
                hashCode = hashCode * -1521134295 + Y.GetHashCode();
                return hashCode;
            }
        }
    }
}