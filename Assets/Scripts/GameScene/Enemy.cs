﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace SpaceInvaders
{
    public class Enemy
    {
        public event Action<Wave.Position, GameObject> Damaged;
        public event Action<Vector3> PositionChanged;
        public event Action EndReached;
        readonly Settings _settings;
        readonly IFireService _fireService;
        readonly Wave.Position _id;
        Vector3 _position;
        Tween _moveDownTween;
        float _movementTimer = 0f;
        int _direction = 1;
        bool _isMoving = true;


        [Inject]
        public Enemy(IFireService fireService, Settings settings, Vector3 position, Wave.Position id)
        {
            _fireService = fireService;
            _settings = settings;
            _position = position;
            _id = id;
        }

        public void HandleCollision(GameObject collided)
        {
            var tag = collided.tag;
            switch (tag)
            {
                case Tags.PlayerProjectile:
                    Damaged?.Invoke(_id, collided);
                    break;
                case Tags.EndGameBlock:
                    EndReached?.Invoke();
                    break;
            }
        }

        public void Tick()
        {
            Move();
            PositionChanged?.Invoke(_position);
        }

        void Move()
        {
            var speed = 1f;
            _movementTimer += Time.deltaTime;
            if (_movementTimer > _settings.timeForMovementInOneDirectionInSeconds)
            {
                _direction *= -1;
                _movementTimer = 0f;
            }
            _position += new Vector3(speed * _direction * Time.deltaTime, 0f, 0f);
        }

        public void Fire()
        {
            _fireService.Fire(_position);
        }

        public void MoveDown(float amount)
        {
            if (_moveDownTween != null)
            {
                _moveDownTween.Kill();
            }
            float movingDownTime = 1.5f;
            float delay = UnityEngine.Random.Range(0f, 0.5f);
            _moveDownTween = DOTween.Sequence()
                .AppendCallback(StopMoving)
                .Insert(delay, DOTween.To(() => _position, (x) => _position = x, _position + new Vector3(0f, 0f, -amount), movingDownTime - delay).SetEase(Ease.OutCubic))
                .InsertCallback(movingDownTime, StartMoving);
        }

        void StopMoving()
        {
            _isMoving = false;
        }

        void StartMoving()
        {
            _isMoving = true;
        }

        [Serializable]
        public class Settings
        {
            [Range(1f, 15f)]
            public float timeForMovementInOneDirectionInSeconds = 5f;
            [Range(1f, 30f)]
            public float projectileSpeed = 7f;
        }

        public class Factory : PlaceholderFactory<Vector3, Wave.Position, Enemy>
        {
            public Transform _enemyParentTransform;

            public Factory(Transform enemyParentTransform)
            {
                _enemyParentTransform = enemyParentTransform;
            }
        }

        public enum Type
        {
            Enemy0, Enemy1, Enemy2
        }
    }
}