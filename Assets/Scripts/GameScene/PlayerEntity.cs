using System;
using UnityEngine;
using Zenject;
using UnityEngine.InputSystem;
using Cysharp.Threading.Tasks;

namespace SpaceInvaders
{
    public class PlayerEntity : IInitializable, ITickable
    {
        public event Action StartBeingInvincible;
        public event Action StopBeingInvincible;
        public event Action<Vector3> PositionChanged;
        public event Action<Quaternion> RotationChanged;
        readonly Settings _settings;
        readonly IFireService _fireService;
        readonly IHealthEntity _healthEntity;
        readonly IHighScoreService _highScoreService;
        readonly IScoreService _scoreService;
        readonly IPlayerMovementEntity _playerMovementEntity;
        Vector3 _position;
        Quaternion _rotation;

        public PlayerEntity(Settings settings,
            IFireService fireService,
            IHealthEntity healthEntity,
            IHighScoreService highScoreService,
            IScoreService scoreService,
            IPlayerMovementEntity playerMovementEntity)
        {
            _settings = settings;
            _fireService = fireService;
            _healthEntity = healthEntity;
            _highScoreService = highScoreService;
            _scoreService = scoreService;
            _playerMovementEntity = playerMovementEntity;
            _position = _settings.startingPosition;
            _rotation = Quaternion.identity;
        }

        public void Initialize()
        {
            InstallFireBindings();
            _scoreService.ResetScore();
            _playerMovementEntity.Rotate += NotifyRotationChanged;
        }

        void NotifyRotationChanged(Quaternion rotation)
        {
            RotationChanged(rotation);
        }

        void InstallFireBindings()
        {
            GetFireAction().performed += Fire;
        }

        void UninstallFireBindings()
        {
            GetFireAction().performed -= Fire;
        }

        InputAction GetFireAction()
        {
            return _settings.controls.FindActionMap("Player").FindAction("Fire");
        }

        void Fire(InputAction.CallbackContext context)
        {
            _fireService.Fire(_position);
        }

        public void TakeDamage()
        {
            var result = _healthEntity.TakeDamage();
            if (result == TakingDamageResult.Alive)
            {
                _healthEntity.StopTakingDamage();
                StartBeingInvincible?.Invoke();
                ScheduleLosingInvincibility();
            }
            else if (result == TakingDamageResult.Dead)
            {
                Die();
            }
        }

        public void HandleCollision(GameObject collided)
        {
            switch (collided.tag)
            {
                case Tags.EnemyProjectile:
                    TakeDamage();
                    UnityEngine.Object.Destroy(collided);
                    break;
                case Tags.Enemy:
                    TakeDamage();
                    break;
            }
        }

        void Die()
        {
            _highScoreService.SaveHighScore(_scoreService.GetScore());
            DisableSystems();
        }

        public void DisableSystems()
        {
            UninstallFireBindings();
            _playerMovementEntity.DisableMovement();
            _playerMovementEntity.Rotate -= NotifyRotationChanged;
        }

        async void ScheduleLosingInvincibility()
        {
            await UniTask.Delay(TimeSpan.FromSeconds(_settings.invincibilityTime));
            _healthEntity.StartTakingDamage();
            StopBeingInvincible?.Invoke();
        }

        public void Tick()
        {
            _position = _playerMovementEntity.Move(_position);
            PositionChanged(_position);
        }

        [Serializable]
        public class Settings
        {
            [Range(3f, 30f)]
            public float Speed;
            public InputActionAsset controls;
            [Range(1, 20)]
            public int startingHealth = 3;
            [Range(1f, 30f)]
            public float projectileSpeed;
            [Range(1f, 5f)]
            public float invincibilityTime = 3f;
            public Vector3 startingPosition;
            [Range(8f, 12f)]
            public float maxHorizontalPosition = 9f;
        }
    }
}