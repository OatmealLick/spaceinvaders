using UnityEngine;

namespace SpaceInvaders
{
    public interface IFireService
    {
        void Fire(Vector3 startingPosition);
    }
}