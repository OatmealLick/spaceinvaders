using UnityEngine;

namespace SpaceInvaders
{
    public class PlayerFireService : IFireService
    {
        readonly ProjectileController.Factory _projectileFactory;
        readonly PlayerEntity.Settings _settings;

        public PlayerFireService(ProjectileController.Factory projectileFactory, PlayerEntity.Settings settings)
        {
            _projectileFactory = projectileFactory;
            _settings = settings;
        }

        public void Fire(Vector3 startingPosition)
        {
            var projectile = _projectileFactory.Create(_settings.projectileSpeed);
            projectile.transform.localPosition = startingPosition;
            projectile.gameObject.tag = Tags.PlayerProjectile;

        }

    }
}