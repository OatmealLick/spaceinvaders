﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Zenject;

namespace SpaceInvaders
{
    public class ScoreComponent : MonoBehaviour
    {
        [SerializeField]
        TextMeshProUGUI scoreText;

        IScoreService _scoreService;

        [Inject]
        public void Construct(IScoreService scoreService)
        {
            _scoreService = scoreService;
            _scoreService.Updated += UpdateScore;
            UpdateScore(_scoreService.GetScore());
        }

        void OnDestroy()
        {
            _scoreService.Updated -= UpdateScore;
        }

        void UpdateScore(int score)
        {
            scoreText.text = score.ToString();
        }
    }
}