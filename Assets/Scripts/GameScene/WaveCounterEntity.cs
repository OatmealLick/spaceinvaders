using System;

namespace SpaceInvaders
{
    public class WaveCounterEntity : IWaveCounterEntity
    {
        public event Action<int> WaveAdded;
        int _waveCounter;

        public WaveCounterEntity()
        {
            _waveCounter = 0;
        }


        public void AddWave()
        {
            _waveCounter++;
            WaveAdded?.Invoke(_waveCounter);
        }

        public int GetWaveCount()
        {
            return _waveCounter;
        }
    }
}