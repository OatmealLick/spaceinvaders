using System;
using UnityEngine;

namespace SpaceInvaders
{
    public class HealthEntity : IHealthEntity
    {
        int _health;
        bool _takingDamage;
        public event Action Died;
        public event Action Damaged;

        public HealthEntity(PlayerEntity.Settings settings)
        {
            _health = settings.startingHealth;
            _takingDamage = true;
        }

        public TakingDamageResult TakeDamage()
        {
            return DoTakeDamage(1);
        }

        private TakingDamageResult DoTakeDamage(int healthPoints)
        {
            if (_takingDamage)
            {
                _health -= healthPoints;
                if (_health > 0)
                {
                    Damaged();
                    return TakingDamageResult.Alive;
                }
                else
                {
                    Died();
                    return TakingDamageResult.Dead;
                }
            }
            else
            {
                return TakingDamageResult.Invincible;
            }
        }

        public void StopTakingDamage()
        {
            _takingDamage = false;
        }

        public void StartTakingDamage()
        {
            _takingDamage = true;
        }

        public int GetHealth()
        {
            return _health;
        }

        public void Die()
        {
            DoTakeDamage(_health);
        }
    }
}