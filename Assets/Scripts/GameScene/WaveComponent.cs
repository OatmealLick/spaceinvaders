﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Zenject;

namespace SpaceInvaders
{
    public class WaveComponent : MonoBehaviour
    {
        [SerializeField]
        TextMeshProUGUI _waveText;

        IWaveCounterEntity _waveCounterEntity;

        [Inject]
        public void Construct(IWaveCounterEntity waveCounterEntity)
        {
            _waveCounterEntity = waveCounterEntity;
            _waveCounterEntity.WaveAdded += UpdateWaveText;
        }

        void OnDestroy()
        {
            _waveCounterEntity.WaveAdded -= UpdateWaveText;
        }

        void UpdateWaveText(int waveCount)
        {
            _waveText.text = waveCount.ToString();
        }
    }
}