﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace SpaceInvaders
{
    public class ExitButtonComponent : MonoBehaviour
    {
        WaveSpawner _waveSpawner;
        PlayerEntity _playerEntity;

        [Inject]
        public void Construct(WaveSpawner waveSpawner, PlayerEntity playerEntity)
        {
            _waveSpawner = waveSpawner;
            _playerEntity = playerEntity;
        }

        public void Exit()
        {
            _waveSpawner.RemoveWave();
            _playerEntity.DisableSystems();
            SceneManager.LoadScene("Menu", LoadSceneMode.Single);
        }
    }

}