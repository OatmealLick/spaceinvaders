using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;
using DG.Tweening;

namespace SpaceInvaders
{
    public class WaveSpawner : ITickable, IInitializable
    {
        readonly Wave.Factory _waveFactory;
        readonly IWaveCounterEntity _waveCounterEntity;
        Wave _wave;

        public WaveSpawner(Wave.Factory waveFactory, IWaveCounterEntity waveCounterEntity)
        {
            _waveFactory = waveFactory;
            _waveCounterEntity = waveCounterEntity;
        }

        public void Initialize()
        {
            CreateNewWave();
        }

        void CreateNewWave()
        {
            _waveCounterEntity.AddWave();
            _wave = _waveFactory.Create();
            _wave.Initialize();
            _wave.WaveDestroyed += ReactToWaveDestroyed;
        }

        public void Tick()
        {
            TickSpawned();
        }

        void TickSpawned()
        {
            _wave?.Tick();
        }

        void ReactToWaveDestroyed()
        {
            CreateNewWave();
        }

        public void RemoveWave()
        {
            _wave.DisableSystems();
            _wave = null;
        }
    }
}