using System;

namespace SpaceInvaders
{
    public interface IWaveCounterEntity
    {
        event Action<int> WaveAdded;
        void AddWave();

        int GetWaveCount();
    }
}