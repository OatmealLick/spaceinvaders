﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace SpaceInvaders
{
    public class EnemyController : MonoBehaviour
    {
        public Enemy _enemy;

        public void SetupEntity(Enemy enemy)
        {
            _enemy = enemy;
            _enemy.PositionChanged += Move;
            _enemy.Damaged += Die;
        }

        void OnDestroy()
        {
            _enemy.PositionChanged -= Move;
            _enemy.Damaged -= Die;
        }

        void OnTriggerEnter(Collider other)
        {
            _enemy.HandleCollision(other.transform.root.gameObject);
        }

        void Move(Vector3 position)
        {
            transform.position = position;
        }

        void Die(Wave.Position id, GameObject collided)
        {
            Destroy(collided);
            Destroy(gameObject);
        }
    }
}