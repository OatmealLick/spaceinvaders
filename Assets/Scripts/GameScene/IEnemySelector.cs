﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceInvaders
{
    public interface IEnemySelector
    {
        Enemy.Type Select(int row, int column);
    }
}