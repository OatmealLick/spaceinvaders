using UnityEngine;
using Zenject;
namespace SpaceInvaders
{

    [CreateAssetMenu(fileName = "GameSettingsInstaller", menuName = "Installers/GameSettingsInstaller")]
    public class GameSettingsInstaller : ScriptableObjectInstaller<GameSettingsInstaller>
    {
        public PlayerEntity.Settings MainCharacter;
        public Enemy.Settings Enemy;
        public Wave.Settings Wave;

        public override void InstallBindings()
        {
            Container.BindInstances(MainCharacter, Enemy, Wave);
        }
    }
}