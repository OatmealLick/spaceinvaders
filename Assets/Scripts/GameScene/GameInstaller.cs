﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace SpaceInvaders
{
    public class GameInstaller : MonoInstaller
    {
        public GameObject ProjectilePrefab;
        public Transform EnemyParentTransform;
        public GameObject Results;
        public GameObject ExitButton;

        public override void InstallBindings()
        {
            Container.Bind<ILowestPositionsSelector>().To<LowestPositionsSelector>().AsSingle();
            Container.BindInterfacesAndSelfTo<ProtectTheKingEnemySelector>().AsSingle();
            Container.BindInterfacesAndSelfTo<PlayerMovementEntity>().AsSingle();
            Container.Bind<IFireService>().To<PlayerFireService>().AsSingle().WhenInjectedInto<PlayerEntity>();
            Container.Bind<IHealthEntity>().To<HealthEntity>().AsSingle();
            Container.Bind<IFireService>().To<EnemyFireService>().AsSingle().WhenInjectedInto<Enemy>();
            Container.Bind<IWaveCounterEntity>().To<WaveCounterEntity>().AsSingle();
            Container.BindInterfacesAndSelfTo<ResultsEnablerService>().AsSingle().WithArguments(Results, ExitButton).NonLazy();
            Container.BindInterfacesAndSelfTo<PlayerEntity>().AsSingle();
            Container.BindInterfacesAndSelfTo<WaveSpawner>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<Enemy>().AsTransient();
            Container.BindFactory<Wave, Wave.Factory>();
            Container.BindFactory<float, ProjectileController, ProjectileController.Factory>().FromComponentInNewPrefab(ProjectilePrefab);
            Container.BindFactory<Vector3, Wave.Position, Enemy, Enemy.Factory>().WithFactoryArguments(EnemyParentTransform);
        }
    }
}