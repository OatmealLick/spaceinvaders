using System;

namespace SpaceInvaders
{
    public interface IScoreService
    {
        event Action<int> Updated;
        void AddPoint();
        int GetScore();
        void ResetScore();
    }
}