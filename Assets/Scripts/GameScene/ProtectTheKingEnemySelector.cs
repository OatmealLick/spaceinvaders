﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace SpaceInvaders
{
    public class ProtectTheKingEnemySelector : IEnemySelector, IInitializable
    {
        readonly Wave.Settings _settings;
        int _topRow;
        int _middleColumn;

        public ProtectTheKingEnemySelector(Wave.Settings settings)
        {
            _settings = settings;
        }

        public void Initialize()
        {
            _middleColumn = _settings.columns / 2;
            _topRow = _settings.rows - 1;
        }

        public Enemy.Type Select(int row, int column)
        {
            int rowDistance = Mathf.Abs(_topRow - row);
            int columnDistance = Mathf.Abs(_middleColumn - column);
            if (columnDistance == 0 && rowDistance == 0)
            {
                return Enemy.Type.Enemy1;
            }
            else if (columnDistance <= 1 && rowDistance <= 1)
            {
                return Enemy.Type.Enemy2;
            }
            else
            {
                return Enemy.Type.Enemy0;
            }
        }
    }
}