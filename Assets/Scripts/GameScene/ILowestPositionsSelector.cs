using System;
using System.Collections.Generic;

namespace SpaceInvaders
{
    public interface ILowestPositionsSelector
    {
        List<Wave.Position> Select(List<Wave.Position> positions);
    }
}