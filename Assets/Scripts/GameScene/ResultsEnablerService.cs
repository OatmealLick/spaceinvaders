﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace SpaceInvaders
{
    public class ResultsEnablerService : IInitializable
    {
        GameObject _results;
        GameObject _exitButton;
        IHealthEntity _healthEntity;

        public ResultsEnablerService(GameObject results, GameObject exitButton, IHealthEntity healthEntity)
        {
            _healthEntity = healthEntity;
            _results = results;
            _exitButton = exitButton;
        }

        void ShowResults()
        {
            _results.SetActive(true);
            _exitButton.SetActive(false);
        }

        public void Initialize()
        {
            _healthEntity.Died += ShowResults;
        }
    }
}