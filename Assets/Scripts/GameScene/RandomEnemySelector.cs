﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceInvaders
{
    public class RandomEnemySelector : IEnemySelector
    {
        private const int _typesCount = 3;
        public Enemy.Type Select(int row, int column)
        {
            return ((Enemy.Type)Random.Range(0, 3));
        }
    }
}