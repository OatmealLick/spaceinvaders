﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;
using DG.Tweening;
using System;

namespace SpaceInvaders
{
    public class PlayerMovementEntity : IPlayerMovementEntity, IInitializable, ITickable
    {
        public event Action<Quaternion> Rotate;
        float _direction;
        Tween rotationTween;
        Quaternion _rotation;
        Quaternion _lastRotation;
        readonly PlayerEntity.Settings _settings;
        private const float AngleToTiltWhileMoving = 30f;
        private const float TimeToTilt = 0.3f;

        public PlayerMovementEntity(PlayerEntity.Settings settings)
        {
            _direction = 0f;
            _settings = settings;
            _rotation = Quaternion.identity;
        }

        public void Initialize()
        {
            InstallMoveBindings();
        }

        void InstallMoveBindings()
        {
            var moveAction = GetMoveAction();
            moveAction.performed += ChangeDirection;
            moveAction.canceled += ChangeDirection;
        }

        void UninstallMoveBindings()
        {
            var moveAction = GetMoveAction();
            moveAction.performed -= ChangeDirection;
            moveAction.canceled -= ChangeDirection;
        }

        InputAction GetMoveAction()
        {
            return _settings.controls.FindActionMap("Player").FindAction("Move");
        }

        public Vector3 Move(Vector3 position)
        {
            if (Mathf.Abs(_direction) > 0.9f)
            {
                var deltaX = _direction * _settings.Speed * Time.deltaTime;
                var newX = Mathf.Clamp(deltaX + position.x, -_settings.maxHorizontalPosition, _settings.maxHorizontalPosition);
                return new Vector3(newX, position.y, position.z);
            }
            return position;
        }

        void ChangeDirection(InputAction.CallbackContext context)
        {
            var newDirection = context.ReadValue<float>();
            ApplyRotation(newDirection);
            _direction = newDirection;
        }

        void ApplyRotation(float newDirection)
        {
            var difference = _direction - newDirection;
            if (IsSignificantInputChange(difference))
            {
                var endRotation = IsStill() ? new Vector3(0f, 0f, AngleToTiltWhileMoving * Mathf.Sign(difference)) : Vector3.zero;
                if (rotationTween != null)
                {
                    rotationTween.Kill();
                }
                rotationTween = DOTween.To(() => _rotation, (delta) => _rotation = delta, endRotation, TimeToTilt);
            }
        }

        bool IsSignificantInputChange(float difference)
        {
            return Mathf.Abs(difference) > 0.9f;
        }

        bool IsStill()
        {
            return Mathf.Abs(_direction) < 0.1f;
        }

        public void DisableMovement()
        {
            UninstallMoveBindings();
            _direction = 0f;
        }

        public void EnableMovement()
        {
            InstallMoveBindings();
        }

        public void Tick()
        {
            if (_lastRotation != _rotation)
            {
                Rotate?.Invoke(_rotation);
            }
            _lastRotation = _rotation;
        }
    }
}