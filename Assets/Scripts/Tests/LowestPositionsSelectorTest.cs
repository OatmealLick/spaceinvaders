using Zenject;
using NUnit.Framework;
using System.Collections.Generic;
namespace SpaceInvaders
{

    [TestFixture]
    public class LowestPositionsSelectorTest : ZenjectUnitTestFixture
    {
        [SetUp]
        public void CommonInstall()
        {
            Container.Bind<LowestPositionsSelector>().AsSingle();
        }

        [Test]
        public void ShouldReturnEmptyDictionaryWhenGotEmptyDictionary()
        {
            var selector = Container.Resolve<LowestPositionsSelector>();

            var actual = selector.Select(CreatePositions());

            Assert.IsEmpty(actual);
        }

        [Test]
        public void ShouldReturnFirstColumnPositionEnemy()
        {
            var expected = CreatePositions(0, 0, 0, 1);
            var selector = Container.Resolve<LowestPositionsSelector>();
            var positions = CreatePositions(0, 0, 1, 0, 0, 1);

            List<Wave.Position> actual = selector.Select(positions);

            Assert.AreEqual(expected, actual);
        }

        List<Wave.Position> CreatePositions(params int[] alternatingPositions)
        {
            if (alternatingPositions.Length % 2 != 0)
            {
                throw new System.Exception("Provide coordinates as x1, y1, x2, y2, ...");
            }

            var counter = 0;
            var positions = new List<Wave.Position>();
            while (counter < alternatingPositions.Length)
            {
                positions.Add(new Wave.Position(alternatingPositions[counter], alternatingPositions[counter + 1]));
                counter += 2;
            }
            return positions;
        }
    }
}