public class Tags
{
    public const string PlayerProjectile = "PlayerProjectile";
    public const string Player = "Player";
    public const string EnemyProjectile = "EnemyProjectile";
    public const string Enemy = "Enemy";
    public const string EndGameBlock = "EndGameBlock";
}