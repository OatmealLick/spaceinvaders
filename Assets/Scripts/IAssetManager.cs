﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace SpaceInvaders
{
    public interface IAssetManager
    {
        event Action<float> ProgressChanged;
        event Action AssetsLoaded;
        AsyncOperationHandle<GameObject> InstantiateEnemy(Enemy.Type type, Vector3 position, Quaternion rotation, Transform parent);
    }
}