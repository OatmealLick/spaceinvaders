﻿using UnityEngine;
using Zenject;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.SceneManagement;
using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;

namespace SpaceInvaders
{
    public class AssetManager : IAssetManager, IInitializable
    {
        public event Action AssetsLoaded;
        public event Action<float> ProgressChanged;

        readonly Wave.Settings _waveSettings;
        readonly object _locker = new object();
        float _progress;

        public AssetManager(Wave.Settings waveSettings)
        {
            _waveSettings = waveSettings;
            _progress = 0f;
        }

        public async void Initialize()
        {
            await UniTask.WhenAll(
                _waveSettings.enemyPrefabs.Select(assetReference => LoadAsset(assetReference))
            );
            AssetsLoaded?.Invoke();
        }

        async UniTask LoadAsset(AssetReference assetReference)
        {
            await (Addressables.LoadAssetAsync<GameObject>(assetReference)).ToUniTask();
            ProgressChanged?.Invoke(ProgressChunk());
        }

        public AsyncOperationHandle<GameObject> InstantiateEnemy(Enemy.Type type, Vector3 position, Quaternion rotation, Transform parent)
        {
            return Addressables.InstantiateAsync(_waveSettings.enemyPrefabs[(int)type], position, rotation, parent);
        }

        float ProgressChunk()
        {
            return 1.0f / _waveSettings.enemyPrefabs.Count;
        }
    }
}