using System.Collections.Generic;

namespace SpaceInvaders
{
    public interface IHighScoreService
    {
        void SaveHighScore(int score);

        List<HighScore> GetHighScores();

        void DeleteAllHighScores();
    }

    public class HighScore
    {
        public int Score { get; private set; }
        public string Date { get; private set; }

        public HighScore(int score, string date)
        {
            Score = score;
            Date = date;
        }

        public override bool Equals(object obj)
        {
            return obj is HighScore score &&
                   Score == score.Score &&
                   Date == score.Date;
        }

        public override int GetHashCode()
        {
            int hashCode = 1548379894;
            hashCode = hashCode * -1521134295 + Score.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Date);
            return hashCode;
        }

        public override string ToString()
        {
            return $"score: {Score}, date: {Date}";
        }
    }
}