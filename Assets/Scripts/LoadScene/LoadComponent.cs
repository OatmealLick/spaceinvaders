﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;
using UnityEngine.UI;

namespace SpaceInvaders
{
    public class LoadComponent : MonoBehaviour
    {
        [SerializeField]
        Slider _slider;
        IAssetManager _assetManager;

        [Inject]
        public void Construct(IAssetManager assetManager)
        {
            _assetManager = assetManager;
        }

        void OnEnable()
        {
            _assetManager.AssetsLoaded += ProceedToMenu;
            _assetManager.ProgressChanged += UpdateProgress;
        }

        void OnDisable()
        {
            _assetManager.AssetsLoaded -= ProceedToMenu;
            _assetManager.ProgressChanged -= UpdateProgress;
        }

        void ProceedToMenu()
        {
            SceneManager.LoadScene("Menu", LoadSceneMode.Single);
        }

        void UpdateProgress(float progressChunk)
        {
            _slider.value += progressChunk;
        }
    }
}