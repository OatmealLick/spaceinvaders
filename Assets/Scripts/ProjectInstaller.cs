using UnityEngine;
using Zenject;

namespace SpaceInvaders
{
    public class ProjectInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IScoreService>().To<ScoreService>().AsSingle();
            Container.Bind<IHighScoreService>().To<PlayerPrefsHighScoreService>().AsSingle();
            Container.BindInterfacesAndSelfTo<FakeWaitAssetManager>().AsSingle();
        }
    }
}